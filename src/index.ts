/*********************************************************************
@name index.ts
@description Servidor express que monta un directorio publico para la
web app e implementa una API REST con Firebase
@author Joan Ciprià Moreno Teodoro
@date 08/09/2019
@license GPLv3
*********************************************************************/

import express from 'express'
import bodyParser from 'body-parser'

let path = require('path');
let app = express();

// Puerto del servidor
const port = 3000;

// Utilizar bodyParser (preciso para leer req.body)
app.use(bodyParser.urlencoded({ extended: false }));

// Carpeta pública para la app web
app.use('/', express.static(path.join(__dirname, './../www')))

// Middleware
app.use((req, res, next) => {
  // Permitir cross origin
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

  // Monitorear todas la peticiones
  console.log(`${new Date().toString()} => ${req.originalUrl}`, req.body)
  next()
})

// Rutas
let measureRoutes = require('./routes/measures');
app.use(measureRoutes);

let usersRoutes = require('./routes/users');
app.use(usersRoutes);

// Ejecutar servidor
app.listen(port, () => console.info("Server running at http://localhost:3000"));
/*********************************************************************
@name measures.ts
@description Servicio Firebase que hace uso de Firebase Server SDK
para guardar datos en la base de datos.
@author Joan Ciprià Moreno Teodoro
@date 09/09/2019
@license GPLv3
*********************************************************************/

// Importar Firebaser Server SDK
import * as FirebaseAdmin from 'firebase-admin'

// Importar api key
//import * as serviceAccount from './proyecto3a-fd2f8-firebase-adminsdk-vkm2n-3388ca76de.json'
var serviceAccount = require("./proyecto3a-fd2f8-firebase-adminsdk-vkm2n-3388ca76de.json");


export class Firebase {
    private db: any;
    private measuresCollection: any;
    private usersCollection: any;
    private databaseURL = "https://proyecto3a-fd2f8.firebaseio.com";

    constructor() {
        if (!FirebaseAdmin.apps.length) {
            FirebaseAdmin.initializeApp({
                credential: FirebaseAdmin.credential.cert(serviceAccount),
                databaseURL: this.databaseURL
            });
        }
        this.db = FirebaseAdmin.firestore();
        this.measuresCollection = this.db.collection("measures");
        this.usersCollection = this.db.collection("users");

    }

    // Añadir medida
    public addMeasure(data) {
        // Procesar datos
        data.value = parseFloat(data.value);
        data.date = parseInt(data.date);
        data.latitude = parseFloat(data.latitude);
        data.longitude = parseFloat(data.longitude);

        // Guardar
        return new Promise((resolve, reject) => {
            this.measuresCollection.add({
                value: data.value,
                date: data.date,
                latitude: data.latitude,
                longitude: data.longitude
            }).then(ref => {
                console.log('Added document with ID: ', ref.id);
                return resolve(ref.id);
            });
        });
    }

    // Añadir medida
    public addUserData(data) {
        // Procesar datos

        // Guardar
        return new Promise((resolve, reject) => {
            this.usersCollection.doc(data.uuid).set({
                name: data.name,
                telephone: data.telephone
            }).then(ref => {
                console.log('Added document with ID: ', ref.id);
                return resolve(ref.id);
            });
        });
    }
}

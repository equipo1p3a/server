/*********************************************************************
@name measures.ts
@description Rutas de medidasd pertenecientes a API REST
@author Joan Ciprià Moreno Teodoro
@date 08/09/2019
@license GPLv3
*********************************************************************/


import express from 'express'
import {Firebase} from '../firebase/index';

// Servicio de Firebase SDK
const firebase = new Firebase();

// Crear router
let router = express.Router();

// Insertar datos usuario
router.post('/api/users', (req, res) => {
   firebase.addUserData(req.body);
   res.sendStatus(200);
})
 
module.exports = router;
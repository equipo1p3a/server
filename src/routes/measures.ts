/*********************************************************************
@name measures.ts
@description Rutas de medidasd pertenecientes a API REST
@author Joan Ciprià Moreno Teodoro
@date 08/09/2019
@license GPLv3
*********************************************************************/


import express from 'express'
import {Firebase} from './../firebase/index';

// Servicio de Firebase SDK
const firebase = new Firebase();

// Crear router
let router = express.Router();

// Insertar nueva medida
router.post('/api/measure', (req, res) => {
   firebase.addMeasure(req.body);
   res.sendStatus(200);
})
 
module.exports = router;
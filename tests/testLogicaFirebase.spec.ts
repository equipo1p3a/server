/*********************************************************************
@name testLogicaFirebase.ts
@description Tests con Mocha de la lógica de Firebase. Ejecutar "npm test"
para hacer los tests.
@author Joan Ciprià Moreno Teodoro
@date 04/11/2019
@license GPLv3
*********************************************************************/

// Importamos lógica de firebase
import { Firebase } from './../src/firebase/index';

// Importamos librerías para los test
import { expect } from 'chai';
import 'mocha';

// Nueva instnacia de la lógica
let firebase = new Firebase();

describe('addMeasure()',
    () => {
        it('should not return empty', async () => {
            let testValue = {
                value: 30,
                date: +new Date(),
                latitude: 1234,
                longitude: 1234
            };
            const result = await firebase.addMeasure(testValue);
            expect(result).to.not.equal("");
        });
    });

describe('addUserData()',
    () => {
        it('should not return empty', async () => {
            let testValue = {
                uuid: "testUuid",
                name: "Mocha Test",
                telephone: 7777
            };
            const result = await firebase.addUserData(testValue);
            expect(result).to.not.equal("");
        });
    });